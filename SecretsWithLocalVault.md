# This Quick Demo will deploy Vault Injection locally.


1 - Install Vault Charts repository
```
$ helm repo add hashicorp https://helm.releases.hashicorp.com
"hashicorp" has been added to your repositories
```

2 - Install Vault in your Kubernetes cluster in Development Mode
```
$ helm install vault hashicorp/vault --set "server.dev.enabled=true"
NAME: vault
LAST DEPLOYED: Tue Apr 20 15:49:44 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Thank you for installing HashiCorp Vault!

Now that you have deployed Vault, you should look over the docs on using
Vault with Kubernetes available here:

https://www.vaultproject.io/docs/


Your release is named vault. To learn more about the release, try:

  $ helm status vault
  $ helm get manifest vault
```

3 - After few minutes, Vault and Agent Injector will be up and running:
```
$ kubectl get all -A
NAMESPACE     NAME                                           READY   STATUS    RESTARTS   AGE
default       pod/vault-0                                    1/1     Running   0          25s
default       pod/vault-agent-injector-7f9fdddbf5-mjq5h      1/1     Running   0          27s
kube-system   pod/calico-kube-controllers-69496d8b75-nt8rt   1/1     Running   1          4d4h
kube-system   pod/calico-node-69c7w                          1/1     Running   1          4d4h
kube-system   pod/calico-node-ddrz6                          1/1     Running   1          4d4h
kube-system   pod/calico-node-k5tmx                          1/1     Running   1          4d4h
kube-system   pod/calico-node-spsmb                          1/1     Running   1          4d4h
kube-system   pod/coredns-74ff55c5b-jb9qg                    1/1     Running   1          4d4h
kube-system   pod/coredns-74ff55c5b-r2z58                    1/1     Running   1          4d4h
kube-system   pod/etcd-master1                               1/1     Running   1          4d4h
kube-system   pod/kube-apiserver-master1                     1/1     Running   4          4d2h
kube-system   pod/kube-controller-manager-master1            1/1     Running   5          4d4h
kube-system   pod/kube-proxy-fllb5                           1/1     Running   1          4d4h
kube-system   pod/kube-proxy-fsvbw                           1/1     Running   1          4d4h
kube-system   pod/kube-proxy-hrdj8                           1/1     Running   1          4d4h
kube-system   pod/kube-proxy-hwx7g                           1/1     Running   1          4d4h
kube-system   pod/kube-scheduler-master1                     1/1     Running   5          4d4h

NAMESPACE     NAME                               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes                 ClusterIP   10.96.0.1        <none>        443/TCP                  4d4h
default       service/vault                      ClusterIP   10.99.28.80      <none>        8200/TCP,8201/TCP        27s
default       service/vault-agent-injector-svc   ClusterIP   10.111.218.211   <none>        443/TCP                  27s
default       service/vault-internal             ClusterIP   None             <none>        8200/TCP,8201/TCP        27s
kube-system   service/kube-dns                   ClusterIP   10.96.0.10       <none>        53/UDP,53/TCP,9153/TCP   4d4h

NAMESPACE     NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system   daemonset.apps/calico-node   4         4         4       4            4           kubernetes.io/os=linux   4d4h
kube-system   daemonset.apps/kube-proxy    4         4         4       4            4           kubernetes.io/os=linux   4d4h

NAMESPACE     NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
default       deployment.apps/vault-agent-injector      1/1     1            1           27s
kube-system   deployment.apps/calico-kube-controllers   1/1     1            1           4d4h
kube-system   deployment.apps/coredns                   2/2     2            2           4d4h

NAMESPACE     NAME                                                 DESIRED   CURRENT   READY   AGE
default       replicaset.apps/vault-agent-injector-7f9fdddbf5      1         1         1       27s
kube-system   replicaset.apps/calico-kube-controllers-69496d8b75   1         1         1       4d4h
kube-system   replicaset.apps/coredns-74ff55c5b                    2         2         2       4d4h

NAMESPACE   NAME                     READY   AGE
default     statefulset.apps/vault   1/1     27s

$ kubectl get statefulset
NAME    READY   AGE
vault   1/1     51s
```

4 - Using first Vault Pod (deployed as DaemonSet), we will enable new __internal__ path, of kv-v2 type: 
```
$ kubectl exec -it vault-0 -- /bin/sh
/ $ vault secrets enable -path=internal kv-v2
Success! Enabled the kv-v2 secrets engine at: internal/
```

5 - Then, we create a simple database credentials demo (username and password data):
```
/ $ vault kv put internal/database/config username="db-readonly-username" password="db-secret-password"
Key              Value
---              -----
created_time     2021-04-20T13:53:52.738616932Z
deletion_time    n/a
destroyed        false
version          1
```

6 - We verify the values recently added:
```
/ $ vault kv get internal/database/config
====== Metadata ======
Key              Value
---              -----
created_time     2021-04-20T13:53:52.738616932Z
deletion_time    n/a
destroyed        false
version          1

====== Data ======
Key         Value
---         -----
password    db-secret-password
username    db-readonly-username
```

7 - Now, we enable Kubernetes authentication:
```
/ $ vault auth enable kubernetes
Success! Enabled kubernetes auth method at: kubernetes/
```

8 - We will add an authentication associated with current namespace __ServiceAccount__:
```
/ $ echo $KUBERNETES_PORT_443_TCP_ADDR
10.96.0.1

/ $ vault write auth/kubernetes/config \
  token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
  kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
  kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

Success! Data written to: auth/kubernetes/config
```

>NOTE: Notice that we used ServiceAccount's token and out Kubernetes cluster CA and its servce IP address.

9 - We now add __read__ access to the path where we added the demo credentials by creating "internal-app" __access policy__. 
```
/ $ vault policy write internal-app - <<EOF
path "internal/data/database/config" {
  capabilities = ["read"]
}
EOF

Success! Uploaded policy: internal-app
```

10 - And we bound a new account "internal-app" with the "default" namespace applying "internal-app" policy recently created. 
```
/ $ vault write auth/kubernetes/role/internal-app \
     bound_service_account_names=internal-app \
     bound_service_account_namespaces=default \
     policies=internal-app \
     ttl=24h
Success! Data written to: auth/kubernetes/role/internal-app

/ $ exit
```

11 - We create a dedicated "internal-app" ServiceAccount:

```
$ cat <<EOF| kubectl create -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: internal-app
EOF

serviceaccount/internal-app created


$ kubectl get serviceaccounts
NAME                   SECRETS   AGE
default                1         4d4h
internal-app           1         4s
vault                  1         5m56s
vault-agent-injector   1         5m56s
```

12 - Now we are ready to deploy a demo application:
```
$ cat <<EOF| kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: orgchart
  labels:
    app: orgchart
spec:
  selector:
    matchLabels:
      app: orgchart
  replicas: 1
  template:
    metadata:
      annotations:
      labels:
        app: orgchart
    spec:
      serviceAccountName: internal-app
      containers:
        - name: orgchart
          image: jweissig/app:0.0.1
EOF

deployment.apps/orgchart created
```

After few seconds demo application will be up and running:

```
$ kubectl get pods
NAME                                    READY   STATUS              RESTARTS   AGE
orgchart-7457f8489d-b7q9l               0/1     ContainerCreating   0          7s
vault-0                                 1/1     Running             0          6m13s
vault-agent-injector-7f9fdddbf5-mjq5h   1/1     Running             0          6m15s

$ kubectl get pods
NAME                                    READY   STATUS    RESTARTS   AGE
orgchart-7457f8489d-b7q9l               1/1     Running   0          44s
vault-0                                 1/1     Running   0          6m50s
vault-agent-injector-7f9fdddbf5-mjq5h   1/1     Running   0          6m52s
```


13 - We will now check if __/vault/secrets__ directory exists on the Pod's container:
```
$ kubectl exec \
 $(kubectl get pod -l app=orgchart -o jsonpath="{.items[0].metadata.name}") \
 --container orgchart -- ls /vault/secrets

ls: /vault/secrets: No such file or directory
command terminated with exit code 1
```

__/vault/secrets__ directory does not exist yet.

14 - We will create a patch to include new anotations that will help Vault-Inject process to identify that this Pod requires extra data to run correctly:
```
$ cat <<EOF>patch-inject-secrets.yml
spec:
  template:
    metadata:
      annotations:
        vault.hashicorp.com/agent-inject: "true"
        vault.hashicorp.com/role: "internal-app"
        vault.hashicorp.com/agent-inject-secret-database-config.txt: "internal/data/database/config"
EOF
```

15 - And we apply this patch to our deployment:
```
$ kubectl patch deployment orgchart --patch "$(cat patch-inject-secrets.yml)"

deployment.apps/orgchart patched
```

After few seconds, a new Pod will be created, with the new annotations:
```
$ kubectl get pods
NAME                                    READY   STATUS     RESTARTS   AGE
orgchart-7457f8489d-b7q9l               1/1     Running    0          73s
orgchart-798cbc6c76-wrcrw               0/2     Init:0/1   0          9s
vault-0                                 1/1     Running    0          7m19s
vault-agent-injector-7f9fdddbf5-mjq5h   1/1     Running    0          7m21s
```

Notice that this time our application Pod requires two containers to run correctly:
```
$ kubectl get pods
NAME                                    READY   STATUS            RESTARTS   AGE
orgchart-7457f8489d-b7q9l               1/1     Running           0          85s
orgchart-798cbc6c76-wrcrw               0/2     PodInitializing   0          21s
vault-0                                 1/1     Running           0          7m31s
vault-agent-injector-7f9fdddbf5-mjq5h   1/1     Running           0          7m33s


$ kubectl get pods
NAME                                    READY   STATUS        RESTARTS   AGE
orgchart-7457f8489d-b7q9l               1/1     Terminating   0          93s
orgchart-798cbc6c76-wrcrw               2/2     Running       0          29s
vault-0                                 1/1     Running       0          7m39s
vault-agent-injector-7f9fdddbf5-mjq5h   1/1     Running       0          7m41s

$ kubectl get pods
NAME                                    READY   STATUS    RESTARTS   AGE
orgchart-798cbc6c76-wrcrw               2/2     Running   0          31s
vault-0                                 1/1     Running   0          7m41s
vault-agent-injector-7f9fdddbf5-mjq5h   1/1     Running   0          7m43s
```

16 - We will now check if __/vault/secrets__ directory exists on the Pod's container:
```
$ kubectl exec $(kubectl get pod -l app=orgchart -o jsonpath="{.items[0].metadata.name}") --container orgchart -- ls /vault/secrets
database-config.txt
```

Now, __/vault/secrets__ directory contains __database-config.txt__, injected by Vault Injector container (the initContainer created in your application's Pod).




