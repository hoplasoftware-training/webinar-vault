
## Default Secrets "Encryption" in Kubernetes

1 - First, let's test default "encryption" applied to secrets. We will simply create a __Secret__ with key _passphrase_ and some secret data:
```
$ kubectl create secret generic unsecure-secret --from-literal=passphrase=topsecret
secret/unsecure-secret created
```

2 - Now, we will create a simple Pod with __etcdctl__ binary. We will use _bitnami's etcd image_ for simplification:

```
$ kubectl run etcd-client --image bitnami/etcd --dry-run=client -o yaml -- sleep 1d >etcd-client.pod.yaml

$ cat tcd-client.pod.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: etcd-client
  name: etcd-client
spec:
  containers:
  - args:
    - sleep
    - 1d
    image: bitnami/etcd
    name: etcd-client
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

We will need to use __kube-apiserver__ client certificates and __etcd__ CA for this to work. Therefore, we prepara a bit our Pod.

```
$ cat <<EOF|kubectl create -f -
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: etcd-client
  name: etcd-client
spec:
  securityContext:
    runAsUser: 0
    runAsGroup: 0
  nodeSelector:
    node-role.kubernetes.io/master: ""
  tolerations:
  - key: "node-role.kubernetes.io/master"
    operator: "Exists"
    effect: "NoSchedule"
  containers:
  - args:
    - sleep
    - 1d
    env:
    - name: ETCD_IP
      value: "$(hostname -I|cut -d' ' -f1)"
    image: bitnami/etcd
    name: etcd-client
    resources: {}
    volumeMounts:
    - mountPath: /etc/kubernetes/pki/
      name: etcd-certs
      readOnly: true
  volumes:
  - hostPath:
      path: /etc/kubernetes/pki/
      type: DirectoryOrCreate
    name: etcd-certs
EOF
```

> __NOTE:__ Notice that we added kubernetes certificates to allow our Pod to communicate with our current __etcd__

Once our _etcd-client_ Pod is up and running, we can connect to it to use __etcdctl__ command. 
```
$ kubectl get pods -o wide --selector="run==etcd-client"
NAME          READY   STATUS    RESTARTS   AGE     IP           NODE      NOMINATED NODE   READINESS GATES
etcd-client   1/1     Running   0          4m49s   10.10.8.75   master1   <none>           <none>
```

For this to work, we will simply execute a shell inside __etcd-client__ namespaces:
```
$ kubectl exec -ti etcd-client -- /bin/bash
root@etcd-client:/opt/bitnami/etcd# 
```

>__NOTE:__ Alternatively you can install __etcdctl__ client locally:
>
>Local etcd-client install
>```
>ETCD_VER=v3.2.32
>
># choose either URL
>$ GOOGLE_URL=https://storage.googleapis.com/etcd
>$ GITHUB_URL=https://github.com/etcd-io/etcd/releases/download
>$ DOWNLOAD_URL=${GITHUB_URL}
>
>$ rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
>
>$ rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test
>
>$ curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
>
>$ tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd-download-test --strip-components=1
>
>$ rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
>
>$ sudo mv /tmp/etcd-download-test/etcdctl /usr/local/bin/etcdctl
>
>$ export ETCD_IP=$(hostname -I|cut -d' ' -f1) # <-- We will use this environment variable to include one of our master nodes IP in the etcdctl request.
>```

3 - Now we can connect to our __etcd__ by using __etcdctl__ command:

First verify your ETCD_IP variable (we will use it on either local installation or using etcd-client Pod):
```
$ kubectl exec -ti etcd-client -- /bin/bash
root@etcd-client:/opt/bitnami/etcd# env
```
Check connection verifying __etcd health__ for example (notice that output will depend on your master nodes IP addresses):
```
root@etcd-client:/opt/bitnami/etcd# ETCDCTL_API=3 etcdctl \
--cert /etc/kubernetes/pki/apiserver-etcd-client.crt \
--key /etc/kubernetes/pki/apiserver-etcd-client.key \
--cacert /etc/kubernetes/pki/etcd/ca.crt \
--endpoints ${ETCD_IP}:2379 \
endpoint health

*** EXPECTED OUTPUT:
192.168.100.131:2379 is healthy: successfully committed proposal: took = 8.568921ms
```

>NOTE: Using local etcd-client installation:
>
>```
># ETCDCTL_API=3 etcdctl \
>--cert /etc/kubernetes/pki/apiserver-etcd-client.crt \
>--key /etc/kubernetes/pki/apiserver-etcd-client.key \
>--cacert /etc/kubernetes/pki/etcd/ca.crt \
>--endpoint ${ETCD_IP}:2379 endpoint health
>
>*** EXPECTED OUTPUT:
>192.168.100.131:2379 is healthy: successfully committed proposal: took = 1.821793ms
>
>*** If we ommit --endpoint argument and use localhost as endpoint, output will be:
>127.0.0.1:2379 is healthy: successfully committed proposal: took = 1.821793ms
>```

4 - Let's retrieve our recently created secret, using __/registry/secrets/default/unsecure-secret__ path (we used "default" namespce):
```
root@etcd-client:/opt/bitnami/etcd# ETCDCTL_API=3 etcdctl \
--cert /etc/kubernetes/pki/apiserver-etcd-client.crt \
--key /etc/kubernetes/pki/apiserver-etcd-client.key \
--cacert /etc/kubernetes/pki/etcd/ca.crt \
--endpoints ${ETCD_IP}:2379 \
get /registry/secrets/default/unsecure-secret

*** EXPECTED OUTPUT:
/registry/secrets/default/unsecure-secret
k8s

v1Secret�
�
unsecure-secret�default"*$a5338941-4576-441b-8a96-e8a8fbddf7e42����z�e
kubectl-createUpdate�v����FieldsV1:3
1{"f:data":{".":{},"f:passphrase":{}},"f:type":{}}

passphrase	topsecret�Opaque�"
```
 As we can observe, password is in clear text and easily reacheable by anyone with access to your filesystem using Pods.


5 - At Kubernetes level, __Secret__ security is managed by __Kubernetes RBAC__. As we are using kubernetes-admin user, we have acces to any secret by default. We are not going to use another user with different access to verify this (check documentation to understand [RBAC authorization layer in Kubernetes](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)):
```
$ kubectl get secret unsecure-secret -o jsonpath='{.data.passphrase}'|base64 -d

*** EXPECTED OUTPUT:
topsecret
```

## Secrets Data Encryption at Rest

Let's continue this lab by preparing __Secrets encyption at Rest__

5 - First, we will create a simple encryption key to encrypt our data. We can use between different encryption algorithms such as aescbc, secretbox or aesgcm, or even add external encryptions schemes. Depending on the choosing algorithm, we will add stronger security, and this will also require different encryption keys lenght. We will use aescbc algorithm for this example, which requires 32-byte keys (we can use 8, 16 and 32):

```
$ echo mysupersecretencryptionpassword|base64

*** EXPECTED OUTPUT:
bXlzdXBlcnNlY3JldGVuY3J5cHRpb25wYXNzd29yZAo=
```

This will be our encryption key.

>NOTE: We can use a random 32-bit encryption key by using:
>```
>head -c 32 /dev/urandom | base64
>```

6 - Prepare a __EncryptionConfiguration__ resource file.
```

$ sudo mkdir /etc/kubernetes/configs


cat <<EOF|sudo tee /etc/kubernetes/configs/encryption-provider.yaml
apiVersion: apiserver.config.k8s.io/v1
kind: EncryptionConfiguration
resources:
  - resources:
    - secrets
    providers:
    - aescbc:
        keys:
        - name: key1
          secret: bXlzdXBlcnNlY3JldGVuY3J5cHRpb25wYXNzd29yZAo=
    - identity: {}
EOF
```

This file defines the different algorithms and external solutions that will be applied for decrypting.

>__NOTE:__ The first provider in the list is used to encrypt resources going into storage. When reading resources from storage each provider that matches the stored data attempts to decrypt the data in order. If no provider can read the stored data due to a mismatch in format or secret key, an error is returned which prevents clients from accessing that resource.


7 - Then we will apply this configuration to __Kube-APIServer__: 

First we create a simple backup.
```
$ sudo cp -p /etc/kubernetes/manifests/kube-apiserver.yaml .
```

And now we edit /etc/kubernetes/manifests/kube-apiserver.yaml with your favorite editor, and after editing file should look like this one (values will depend on your environment): 
```
apiVersion: v1
kind: Pod
metadata:
  annotations:
    kubeadm.kubernetes.io/kube-apiserver.advertise-address.endpoint: 10.10.10.11:6443
  creationTimestamp: null
  labels:
    component: kube-apiserver
    tier: control-plane
  name: kube-apiserver
  namespace: kube-system
spec:
  containers:
  - command:
    - kube-apiserver
    - --advertise-address=10.10.10.11
    - --allow-privileged=true
    - --authorization-mode=Node,RBAC
    - --client-ca-file=/etc/kubernetes/pki/ca.crt
    - --enable-admission-plugins=NodeRestriction
    - --encryption-provider-config=/etc/kubernetes/configs/encryption-provider.yaml #<--Add your encryption configuration
    - --enable-bootstrap-token-auth=true
    - --etcd-cafile=/etc/kubernetes/pki/etcd/ca.crt
    - --etcd-certfile=/etc/kubernetes/pki/apiserver-etcd-client.crt
    - --etcd-keyfile=/etc/kubernetes/pki/apiserver-etcd-client.key
    - --etcd-servers=https://127.0.0.1:2379
    - --insecure-port=0
    - --kubelet-client-certificate=/etc/kubernetes/pki/apiserver-kubelet-client.crt
    - --kubelet-client-key=/etc/kubernetes/pki/apiserver-kubelet-client.key
    - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
    - --proxy-client-cert-file=/etc/kubernetes/pki/front-proxy-client.crt
    - --proxy-client-key-file=/etc/kubernetes/pki/front-proxy-client.key
    - --requestheader-allowed-names=front-proxy-client
    - --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt
    - --requestheader-extra-headers-prefix=X-Remote-Extra-
    - --requestheader-group-headers=X-Remote-Group
    - --requestheader-username-headers=X-Remote-User
    - --secure-port=6443
    - --service-account-issuer=https://kubernetes.default.svc.cluster.local
    - --service-account-key-file=/etc/kubernetes/pki/sa.pub
    - --service-account-signing-key-file=/etc/kubernetes/pki/sa.key
    - --service-cluster-ip-range=10.96.0.0/12
    - --tls-cert-file=/etc/kubernetes/pki/apiserver.crt
    - --tls-private-key-file=/etc/kubernetes/pki/apiserver.key
    image: k8s.gcr.io/kube-apiserver:v1.20.6
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 10.10.10.11
        path: /livez
        port: 6443
        scheme: HTTPS
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    name: kube-apiserver
    readinessProbe:
      failureThreshold: 3
      httpGet:
        host: 10.10.10.11
        path: /readyz
        port: 6443
        scheme: HTTPS
      periodSeconds: 1
      timeoutSeconds: 15
    resources:
      requests:
        cpu: 250m
    startupProbe:
      failureThreshold: 24
      httpGet:
        host: 10.10.10.11
        path: /livez
        port: 6443
        scheme: HTTPS
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    volumeMounts:
    - mountPath: /etc/ssl/certs
      name: ca-certs
      readOnly: true
    - mountPath: /etc/ca-certificates
      name: etc-ca-certificates
      readOnly: true
    - mountPath: /etc/pki
      name: etc-pki
      readOnly: true
    - mountPath: /etc/kubernetes/pki
      name: k8s-certs
      readOnly: true
    - mountPath: /usr/local/share/ca-certificates
      name: usr-local-share-ca-certificates
      readOnly: true
    - mountPath: /usr/share/ca-certificates
      name: usr-share-ca-certificates
      readOnly: true
    - mountPath: /etc/kubernetes/configs #<-- Mount new configurations folder 
      name: k8s-configs #<-- Mount new configurations folder
      readOnly: true #<-- Mount new configurations folder
  hostNetwork: true
  priorityClassName: system-node-critical
  volumes:
  - hostPath:
      path: /etc/ssl/certs
      type: DirectoryOrCreate
    name: ca-certs
  - hostPath:
      path: /etc/ca-certificates
      type: DirectoryOrCreate
    name: etc-ca-certificates
  - hostPath:
      path: /etc/pki
      type: DirectoryOrCreate
    name: etc-pki
  - hostPath:
      path: /etc/kubernetes/pki
      type: DirectoryOrCreate
    name: k8s-certs
  - hostPath:
      path: /usr/local/share/ca-certificates
      type: DirectoryOrCreate
    name: usr-local-share-ca-certificates
  - hostPath:
      path: /usr/share/ca-certificates
      type: DirectoryOrCreate
    name: usr-share-ca-certificates
  - hostPath: #<-- Mount new configurations folder
      path: /etc/kubernetes/configs #<-- Mount new configurations folder
      type: DirectoryOrCreate #<-- Mount new configurations folder
    name: k8s-configs #<-- Mount new configurations folder
status: {}
```

After few seconds, a new pod will be created:
```
$ kubectl get pods -A

*** EXPECTED OUTPUT:
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-7854b85cf7-s99k6   1/1     Running   1          5d22h
kube-system   calico-node-6vwxv                          1/1     Running   0          76m
kube-system   calico-node-t7rds                          1/1     Running   0          76m
kube-system   coredns-74ff55c5b-q8dx8                    1/1     Running   1          5d22h
kube-system   coredns-74ff55c5b-zpb5m                    1/1     Running   1          5d22h
kube-system   etcd-master                                1/1     Running   2          5d22h
kube-system   kube-apiserver-master                      1/1     Running   0          54s
kube-system   kube-controller-manager-master             1/1     Running   4          5d22h
kube-system   kube-proxy-ccvld                           1/1     Running   1          5d22h
kube-system   kube-proxy-tlhp7                           1/1     Running   1          5d22h
kube-system   kube-scheduler-master                      1/1     Running   4          5d22h
```


8 - We create now a new "more secure" secret:
```
$ kubectl create secret generic moresecure-secret \
--from-literal=passphrase=anothersecurepassword

*** EXPECTED OUTPUT:
secret/moresecure-secret created
```

This new secret will be encrypted with __aescbc algorithm using the configured key__.


9 - If we take a look at this secret directly in __etcd__:
```
$ kubectl exec -ti etcd-client -- /bin/bash

root@etcd-client:/opt/bitnami/etcd# ETCDCTL_API=3 etcdctl \
--cert /etc/kubernetes/pki/apiserver-etcd-client.crt \
--key /etc/kubernetes/pki/apiserver-etcd-client.key \
--cacert /etc/kubernetes/pki/etcd/ca.crt \
--endpoints ${ETCD_IP}:2379 \
get /registry/secrets/default/moresecure-secret

*** EXPECTED OUTPUT:

/registry/secrets/default/moresecure-secret
k8s:enc:aescbc:v1:key1:L��0�
                            �����8Q��HM����]�����JE�7=��d���OU��s#+ӏoQcZ�V�_��GI͋.Lܟ��h�A����>��=������e���~��`��tT�dKV��Hy�Y���	�]������wG���Wr�ynj��b�<3�S��?�녃|,[��.N��.ӈ{/�@�Z�@>*4��(�Y��ݤ��b
                                                                                  #�q����N���n�a��z�����ﭮ.��|�\Gݮ�F��5b~���
         l��߬�Ô(Wr���=��*t"�6�
```

As expected, output is encrypted this time. __This secret is encrypted in etcd.__

10 - Now let's read this secret using Kubernetes API:
```
$ kubectl get secret moresecure-secret -o jsonpath='{.data.passphrase}'|base64 -d

*** EXPECTED OUTPUT:
anothersecurepassword
```

__We verified that even if we encrypt data at etcd, secrets can be read if RBAC permissions are wrong for example.__
We will need to add an external secure store to manage secrets out of Kubernetes security.
