# Vault Webinar

This repository contains everything used on webinar-vault published on 29th of April 2021:

- [SecretsWithoutVault.md](SecretsWithoutVault.md) - Managing Secrets with Kubernetes configurations, using encryption at Rest on etcd.

- [SecretsWithLocalVault.md](SecretsWithLocalVault.md) - Managing Secrets using a local Vault deployment and Vault Injection for Kubernetes.

- [SecretsWithHCPVault.md](SecretsWithHCPVault.md) - Managing Secrets using a Hashicorp Cloupd Platform Vault cluster and Vault Injection for Kubernetes (this requires a Kubernetes cluster deployed on AWS).

- [InjectingVaultSecretsOnKubernetesSlides.pdf](InjectingVaultSecretsOnKubernetesSlides.pdf) - Slides used for this event.
