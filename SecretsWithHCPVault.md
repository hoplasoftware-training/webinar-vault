### Previous:
- We have a Kubernetes cluster up and running.




### 1 - Create Authorizations for Vault Account.

First we will create an account "vault-auth".

```
$ kubectl create serviceaccount vault-auth

*** EXPECTED OUTPUT:
serviceaccount/vault-auth created
```

We then create a ClusterRoleBinding with system:auth-delegator ClusterRole for vault-auth ServiceAccount. Take into account the namespace or namespaces where your account should be allowed to inject containers. For simplicity we are using "default" namespace to create resources in this demo.

```
$ cat<< EOF| kubectl create -f -
 apiVersion: rbac.authorization.k8s.io/v1
 kind: ClusterRoleBinding
 metadata:
    name: role-tokenreview-binding
    namespace: default
 roleRef:
    apiGroup: rbac.authorization.k8s.io
    kind: ClusterRole
    name: system:auth-delegator
 subjects:
  - kind: ServiceAccount
    name: vault-auth
    namespace: default
EOF


*** EXPECTED OUTPUT:
clusterrolebinding.rbac.authorization.k8s.io/role-tokenreview-binding created

```


### 2 - Prepare your Environment

>__NOTE: YOU MUST USE YOUR OWN VAULT_TOKEN, VAULT_ADDR, VAULT_TLS_SERVER_NAME, values shown are just representative.__

We will prepara a environment file for quick access to some variables.
```
$ cat <<EOF >env.sh
#!/bin/bash
export VAULT_NAMESPACE="admin"
export VAULT_TOKEN="s.dtNYLQ5jtARY0pmY6IHbeWTi.rGGCy"
export VAULT_ADDR="https://labs-vault-cluster.private.vault.c0be0d49-5764-4943-9825-59e7864f7ab2.aws.hashicorp.cloud:8200"

export VAULT_EXTERNAL_ADDR="external-vault"
export VAULT_TLS_SERVER_NAME=labs-vault-cluster.private.vault.c0be0d49-5764-4943-9825-59e7864f7ab2.aws.hashicorp.cloud


export VAULT_SA_NAME=\$(kubectl get sa vault-auth -o jsonpath="{.secrets[*]['name']}")
export SA_JWT_TOKEN=\$(kubectl get secret \$VAULT_SA_NAME -o jsonpath="{.data.token}" | base64 --decode; echo)
export SA_CA_CRT=\$(kubectl get secret \$VAULT_SA_NAME -o jsonpath="{.data['ca\.crt']}" | base64 --decode | tee SA_CA_CRT.crt)

echo "# VAULT_NAMESPACE: \${VAULT_NAMESPACE}"
echo "# VAULT_TOKEN: \${VAULT_TOKEN}"
echo "# VAULT_ADDR: \${VAULT_ADDR}"
echo "# VAULT_SA_NAME: \${VAULT_SA_NAME}"
echo "# SA_JWT_TOKEN: \${SA_JWT_TOKEN}"
echo "# SA_CA_CRT: \${SA_CA_CRT}"
EOF
```

We will then load that variables in our environment:
```
source env.sh
```


### 4 - This step will not work if using Hashicorp Cloud Platform's Vault. We show here for understanding, but we will not use an external-name Service for this demo.

We will create external-name service

>__NOTE:__ Use your own private or public Vault address/FQDN

```
export EXTERNAL_VAULT_ADDR="$(host labs-vault-cluster.private.vault.c0be0d49-5764-4943-9825-59e7864f7ab2.aws.hashicorp.cloud|rev|cut -d' ' -f1|rev)"

cat <<EOF | kubectl apply --filename=-
---
apiVersion: v1
kind: Service
metadata:
  name: external-vault
  namespace: default
spec:
  ports:
  - protocol: TCP
    port: 8200
---
apiVersion: v1
kind: Endpoints
metadata:
  name: external-vault
subsets:
  - addresses:
      - ip: $EXTERNAL_VAULT_ADDR
    ports:
      - port: 8200
EOF


*** EXPECTED OUTPUT:
service/external-vault created
endpoints/external-vault created

```


### 5 - Install helm and add Hashicorp Charts repository

```
$ wget https://get.helm.sh/helm-v3.5.4-linux-amd64.tar.gz \
&& tar -zxvf helm-v3.5.4-linux-amd64.tar.gz \
&& sudo mv linux-amd64/helm /usr/local/bin/ \
&& rm -rf linux-amd64 helm-v3.5.4-linux-amd64.tar.gz
```

Add Hashicorp repository

```
$ helm repo add hashicorp https://helm.releases.hashicorp.com
```

### 6 - Create certificates for Vault-Injector

First we will create a CA to sign our certificate. We will create a key for this CA:
```
$ openssl genrsa -out injector-ca.key 2048

*** EXPECTED OUTPUT:
Generating RSA private key, 2048 bit long modulus (2 primes)
...+++++
.................................+++++
e is 65537 (0x010001)
```

Then we will create cert for this CA:
```
$ openssl req \
-x509 \
-new \
-nodes \
-key injector-ca.key \
-sha256 \
-days 1825 \
-out injector-ca.crt \
-subj "/C=US/ST=CA/L=San Francisco/O=HashiCorp/CN=vault-agent-injector-svc"
```

Now we create a key for vault-injector:
```
$ openssl genrsa -out tls.key 2048

*** EXPECTED OUTPUT:
Generating RSA private key, 2048 bit long modulus (2 primes)
.......+++++
...............+++++
e is 65537 (0x010001)
```

Then we will create a signing request for vault-injector including our CA:
```
$ openssl req \
-new \
-key tls.key \
-out tls.csr \
-subj "/C=US/ST=CA/L=San Francisco/O=HashiCorp/CN=vault-agent-injector-svc"
```

To be able to identify vault-injector on different namespaces, we should create a signing requests containing all possible FQDN names allowed fot vault-injector. For this to work, we will create a configuration file to extend our request properties:
```
$ cat <<EOF >csr.conf
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = vault-agent-injector-svc
DNS.2 = vault-agent-injector-svc.default
DNS.3 = vault-agent-injector-svc.default.svc
DNS.4 = vault-agent-injector-svc.default.svc.cluster.local
EOF
```
>__NOTE:__ We are using "default" namespace to simplify this example. 

Finally, we will create a certificate for vault-injector key, signed by our CA:
```
$ openssl x509 \
-req \
-in tls.csr \
-CA injector-ca.crt \
-CAkey injector-ca.key \
-CAcreateserial \
-out tls.crt \
-days 1825 \
-sha256 \
-extfile csr.conf

*** EXPECTED OUTPUT:
Signature ok
subject=C = US, ST = CA, L = San Francisco, O = HashiCorp, CN = vault-agent-injector-svc
Getting CA Private Key
```

To use the certificate in Kubernetes, we create a secret for vault-injector authorization:
```
$ kubectl create secret generic injector-tls \
--from-file tls.crt \
--from-file tls.key \
--namespace=default
```

### 6 - Vault Injector Installation

Using our previously defined CA, we will install vault-injector only using Helm. We disabled vault-server installation because we will be using an external Vault, in this case deployed on Hashicorp Cloud Platform:
```
$ export CA_BUNDLE=$(cat injector-ca.crt | base64)

$ helm install vault hashicorp/vault \
  --set="injector.externalVaultAddr=https://labs-vault-cluster.private.vault.c0be0d49-5764-4943-9825-59e7864f7ab2.aws.hashicorp.cloud:8200" \
  --set="injector.certs.secretName=injector-tls" \
  --set="injector.certs.caBundle=${CA_BUNDLE?}" \
  --set="server.enabled=false"
```
>__NOTE:__


### 7 - Vault Client Installation

On any node within our cluster or any other with access to our Vault server deployed on Hashicorp Cloud Platform, we will install a simple vault client. 
As we are running this demo on Ubuntu, we will use Ubuntu packages for simplicity:
```
$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

$ sudo apt-get update -qq && sudo apt-get install -qq vault
```


### 8 - Configure our external Vault Server deployed on Hashicorp Cloud Platform to allow our Kubernetes Cluster interaction

To allow Kubernetes to access our Vault Server, we need to create an __auth/kubernetes/config__ configuration with our Kubernetes cluster credentials.
We will use following variables:

- __KUBEHOST__ - Pointing to the IP address of our Kubernetes Cluster (PVC internal IP address because we have created peering communications between our AWS environment and Hashicorp CLoud Platform's Vault PVC)

- __SA_JWT_TOKEN__ - This is the token associated with our __vault-auth__ Service Account


>NOTE: We should have loaded SA_JWT_TOKEN required variable on second step, if it is not already loaded, execute ___source env.sh___. 
```
$ export KUBEHOST=$(hostname -I|cut -d' ' -f1)

$ vault write auth/kubernetes/config  \
token_reviewer_jwt="${SA_JWT_TOKEN}" \
kubernetes_host="https://${KUBEHOST}:6443" \
kubernetes_ca_cert=@SA_CA_CRT.crt 

*** EXPECTED OUTPUT:
Success! Data written to: auth/kubernetes/config
```

### 9 Configure some Vault Secrets for our demo

First we will create __internal__ path of type __kv-v2__:
```
$ vault secrets enable -path=internal kv-v2

*** EXPECTED OUTPUT:
Success! Enabled the kv-v2 secrets engine at: internal/
```

Once __internal__ path is created, we are able to create our demo database credentials:
```
$ vault kv put internal/database/config \
username="db-readonly-username" \
password="db-secret-password"

*** EXPECTED OUTPUT:
Key              Value
---              -----
created_time     2021-04-20T08:22:22.208723382Z
deletion_time    n/a
destroyed        false
version          1
```

We can check demo database credentials:
```
$ vault kv get internal/database/config
====== Metadata ======
Key              Value
---              -----
created_time     2021-04-20T08:22:22.208723382Z
deletion_time    n/a
destroyed        false
version          1

====== Data ======
Key         Value
---         -----
password    db-secret-password
username    db-readonly-username
```

### 11 - Create a ServiceAccount for our application

We create a ServiceAccount in Kubernetes to bound demo database credentials acces from Vault Injector:
```
cat <<EOF| kubectl create -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: internal-app
EOF

*** EXPECTED OUTPUT:
serviceaccount/internal-app created
```

We can easily check
```
$ kubectl get serviceaccounts
NAME                   SECRETS   AGE
default                1         85m
internal-app           1         110s
vault                  1         20m
vault-agent-injector   1         20m
vault-auth             1         58m
```

### 12 - Allow injector to read demo database credentials

We will create a simple policy allowing demo database credentials reading access:
```
$ vault policy write internal-app - <<EOF
path "internal/data/database/config" {
   capabilities = ["read"]
}
EOF

*** EXPECTED OUTPUT:
Success! Uploaded policy: internal-app
```

And we bound this policy to the previously created ServiceAccount and "default" namespace (in which application will be running for this quick demo)
```
$ vault write auth/kubernetes/role/internal-app \
bound_service_account_names=internal-app \
bound_service_account_namespaces=default \
policies=internal-app \
ttl=24h
```

*** EXPECTED OUTPUT:
Success! Data written to: auth/kubernetes/role/internal-app


### 13 - Deploy a simple demo application

We will just create a simple demo application:
```
$ cat <<EOF| kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: orgchart
  labels:
    app: orgchart
spec:
  selector:
    matchLabels:
      app: orgchart
  replicas: 1
  template:
    metadata:
      annotations:
      labels:
        app: orgchart
    spec:
      serviceAccountName: internal-app
      containers:
        - name: orgchart
          image: jweissig/app:0.0.1
EOF
```

We will ensure that application is running by checking its Pods:
``` 
$ kubectl get pods

*** EXPECTED OUTPUT:
NAME                                   READY   STATUS    RESTARTS   AGE
orgchart-7457f8489d-dqjg9              1/1     Running   0          14s
vault-agent-injector-748b9994b-7tnzd   1/1     Running   0          21m
```

### 14 - Check Vault Secrets existence on demo application Pods

We can easily verify that Vault Secrets are not present by default on our demo application as we need a few tweaks (annotations) in our Deployment:
```
$ kubectl exec \
$(kubectl get pod -l app=orgchart -o jsonpath="{.items[0].metadata.name}") \
--container orgchart -- ls /vault/secrets

*** EXPECTED OUTPUT:
ls: /vault/secrets: No such file or directory
command terminated with exit code 1
```


### 15 - Configure aour demo application to use Vault Secrets

We will patch our current demo application deployment to include required annotations to inject Vault Secrets into our Pod.

First create a patch file including following annotations:

- __vault.hashicorp.com/agent-inject: "true"__ - This indicates that injector agent should run inside our Pod as init-container to supply Vault Secrets.

- __vault.hashicorp.com/role: "internal-app"__ - This is the account/role that will be used to retrieve information from Vault (we added read access to required keys to internal-app ServiceAccount).

- __vault.hashicorp.com/namespace: "admin"__ - This is the namespace in which we stored information in Vault (we are using defaults "admin" namespace provided by Hashicorp Cloud Platform Vautl server).

- __vault.hashicorp.com/agent-inject-secret-database-config.txt: "internal/data/database/config"__ - This is the data path where required information is stored. Vault Secrets will be created in our application container in __/vault/secrets__, with the name of the defined after __"agent-inject-secret-"__ label, in this example we will have __database-config.txt__

We create a file with required patch information:
```
$ cat <<EOF>patch-inject-secrets.yml
spec:
  template:
    metadata:
      annotations:
        vault.hashicorp.com/agent-inject: "true"
        vault.hashicorp.com/role: "internal-app"
        vault.hashicorp.com/namespace: "admin"
        vault.hashicorp.com/agent-inject-secret-database-config.txt: "internal/data/database/config"
EOF
```
And we apply this patch to our demo application deployment:
```
$ kubectl patch deployment orgchart --patch "$(cat patch-inject-secrets.yml)"
```

### 16 - Check again Vault Secrets existence on demo application Pods

First thing to notice now is that we have 2 containers inside our demo application Pods:
```
$ kubectl get pods

*** EXPECTED OUTPUT:
NAME                                    READY   STATUS    RESTARTS   AGE
orgchart-86d5f46c-p2z65                 2/2     Running   0          110m
vault-agent-injector-68767974d9-k67bt   1/1     Running   0          116m
```

And now we can verify that our Vault Secret is created on our demo application Pod:
```
$ kubectl exec \
$(kubectl get pod -l app=orgchart -o jsonpath="{.items[0].metadata.name}") \
--container orgchart -- ls /vault/secrets

*** EXPECTED OUTPUT:
database-config.txt
```

In fact, we can read the contents of this file and verify that there is all required information for our demo application:
```
$ kubectl exec $(kubectl get pod -l app=orgchart -o jsonpath="{.items[0].metadata.name}") \
--container orgchart -- cat /vault/secrets/database-config.txt

*** EXPECTED OUTPUT:
data: map[password:db-secret-password username:db-readonly-username]
metadata: map[created_time:2021-04-20T11:03:52.060384547Z deletion_time: destroyed:false version:2]
```

### 17 - USe a Template Annotation to be able to use Vault Secrets in our applications

We create a new patch to format our demo application credentials for accesibility: 
```
$ cat <<EOF>patch-inject-secrets-as-template.yml
spec:
  template:
    metadata:
      annotations:
        vault.hashicorp.com/agent-inject: "true"
        vault.hashicorp.com/agent-inject-status: "update"
        vault.hashicorp.com/role: "internal-app"
        vault.hashicorp.com/namespace: "admin"
        vault.hashicorp.com/agent-inject-secret-database-config.txt: "internal/data/database/config"
        vault.hashicorp.com/agent-inject-template-database-config.txt: |
          {{- with secret "internal/data/database/config" -}}
          postgresql://{{ .Data.data.username }}:{{ .Data.data.password }}@postgres:5432/wizard
          {{- end -}}
EOF
```

And we apply this patch to our demo application deployment:
```
$ kubectl patch deployment orgchart --patch "$(cat patch-inject-secrets-as-template.yml)"
```

After few seconds, patch will be applied and a new demo application Pod will be created.
If we now retrieve the information written inside /vault/secrets/database-config.txt, we will read a connection chain, formated with the defined template and the data retrieved from Vault:
```
kubectl exec \
    $(kubectl get pod -l app=orgchart -o jsonpath="{.items[0].metadata.name}") \
    -c orgchart -- cat /vault/secrets/database-config.txt

*** EXPECTED OUTPUT:
postgresql://db-readonly-user:db-secret-password@postgres:5432/wizard
```


### Final Thoughts - If you reached this step you should have learned how to use Vault Injector to provide Secrets managed on Vault (in this case on an external Vault running in Hashicorp Cloud Platform) to improve your Kubernetes applications security.
